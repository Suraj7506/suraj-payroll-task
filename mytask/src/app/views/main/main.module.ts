import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTaskComponent } from './add-task/add-task.component';



@NgModule({
  declarations: [
    AddTaskComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MainModule { }
