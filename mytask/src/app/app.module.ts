import { InterceptorInterceptor } from './../core/interceptor/interceptor.interceptor';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatButtonModule} from '@angular/material/button';
import { ToastrModule } from 'ngx-toastr';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { CustomDatePipe } from 'src/core/pipe/custom-date.pipe';
import { DeactivateGuard } from 'src/core/auth/deactivate/deactivate.guard';


@NgModule({
  declarations: [
    AppComponent,
    CustomDatePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LoadingBarModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    ToastrModule.forRoot(),
    LoadingBarModule
  ],
  providers: [ DeactivateGuard,InterceptorInterceptor,
    {
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptorInterceptor,
			multi: true
		},],
  bootstrap: [AppComponent]
})
export class AppModule { }
